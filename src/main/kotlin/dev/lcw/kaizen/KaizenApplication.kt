package dev.lcw.kaizen

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KaizenApplication

fun main(args: Array<String>) {
	runApplication<KaizenApplication>(*args)
}
