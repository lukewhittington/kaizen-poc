package dev.lcw.kaizen

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.http.MediaType.TEXT_HTML
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.router
import reactor.core.publisher.Mono
import org.springframework.boot.info.BuildProperties
import org.springframework.beans.factory.annotation.Autowired



@Configuration
class RouteHandler(private val handler: RouteController)
{
    @Bean
    fun roomsRouter() = router {
        (accept(TEXT_HTML) and "/").nest {
            GET("/", handler::index)
        }
    }
}

@Component
class RouteController {
    @Autowired
    var buildProperties: BuildProperties? = null

    fun index(request: ServerRequest): Mono<ServerResponse> {
        return ServerResponse.ok()
                .contentType(MediaType.TEXT_PLAIN)
                .syncBody(buildProperties!!.version)
    }
}
