/* Disable concurrent builds to prevent concurrent Maven builds in the same workspace and discard old builds. */
properties([buildDiscarder(logRotator(artifactDaysToKeepStr: '30', artifactNumToKeepStr: '30', daysToKeepStr: '30', numToKeepStr: '30')), disableConcurrentBuilds()])

final long startTime = System.currentTimeMillis()

/* The JVM options for Maven. TieredCompilation should improve JVM startup speed (see https://zeroturnaround.com/rebellabs/your-maven-build-is-slow-speed-it-up/). */
final String mavenJvmOptions = '-XX:+TieredCompilation -XX:TieredStopAtLevel=1 -Dorg.jenkinsci.plugins.pipeline.maven.eventspy.JenkinsMavenEventSpy.disabled=true'

/* Maven settings file configured in Jenkins with server credentials. */
final String mavenSettings = '6477bc0c-502b-4247-ac2e-dd8f0e62d61d'

/* Name of centrally configured Maven installation in Jenkins. */
final String mavenToolName = 'Maven 3.5.4'

/* Name of centrally configured JDK installation in Jenkins. */
final String jdkToolName = 'Zulu OpenJDK 11'

/* Name of branch being built. */
final String branch = env['BRANCH_NAME']

/* Name of job (URL encoded by Jenkins, so we decode it). */
final String jobName = URLDecoder.decode(env['JOB_NAME'], "UTF-8")

/* Name of build. */
final String buildName = jobName + ' #' + env['BUILD_NUMBER']

/* Maven Options
 *             --batch-mode : recommended in CI to inform maven to not run in interactive mode (less logs).
 *                       -V : strongly recommended in CI, will display the JDK and Maven versions in use.
 *                       -U : force maven to update snapshots each time (default : once an hour, makes no sense in CI).
 * -Dsurefire.useFile=false : useful in CI. Displays test errors in the logs (instead of having to crawl the workspace to see the cause).
 *                    -P ci : Enable ci (continuous integration) profile.
 */
String mavenOptions = '--batch-mode -V -U -e -Dsurefire.useFile=false -Dorg.jenkinsci.plugins.pipeline.maven.eventspy.JenkinsMavenEventSpy.disabled=true -P ci -Dsonar.host.url=http://sonar.cognitran.com:9000'

/* SCM URL */
final scmUrl = 'git@bitbucket.org:cognitranlimited/lukewhittington/kaizen-poc'

/* The Jenkins Docker credentials ID. */
final String dockerCredentialsId = 'ecr:eu-west-1:AWSECR'

/* URL for Docker repository. */
final String dockerUrl = "457832871702.dkr.ecr.eu-west-1.amazonaws.com/products-monitor"

/* The prefix for branch specific docker tags. */
final String dockerTagPrefix = branch.replaceAll("[^A-Za-z0-9_\\-.]", "_")

/* Recipients to send success/failure/release notifications to (space separated list of email addresses). */
final List notificationRecipients = ["luke.whittington@cognitran.com"]
notificationRecipients.addAll(Arrays.asList(((String) emailextrecipients([
        [$class: 'CulpritsRecipientProvider'],
        [$class: 'DevelopersRecipientProvider'],
        [$class: 'RequesterRecipientProvider']
])).split('\\s+')))
notificationRecipients.unique()

/* Whether this is a release build. */
boolean releaseBuild = false

/** The version being built. */
String version = null

/** The Git commit hash. */
String gitCommit = null

// Feature branches should use the "feature" label so that non-feature builds can be priorisied over feature branches.
node('products && linux' + (branch.startsWith("feature/") ? ' && feature' : '')) {
    timeout(time:3, unit: 'HOURS'){
        try {
            stage('Checkout'){
                // Git checkout the branch.
                checkout scm

                // Set build description to commit hash for convenience.
                gitCommit = sh(returnStdout: true, script: 'git rev-parse --short HEAD').trim()
                currentBuild.description = gitCommit

                bitbucketStatusNotify(buildState: 'INPROGRESS')

                // Make sure that committer and author are included in notification emails.
                notificationRecipients.add(sh(returnStdout: true, script: 'git --no-pager log -1 --pretty=format:"%ae"').trim())
                notificationRecipients.add(sh(returnStdout: true, script: 'git --no-pager log -1 --pretty=format:"%ce"').trim())
                notificationRecipients.unique()
                print "Notification Recipients: " + notificationRecipients.join(" ")
            }

            final Closure dockerStages = { ->
                stage('Docker Build') {
                    withDockerRegistry([url: "https://${dockerUrl}", credentialsId: dockerCredentialsId]) {
                        sh('''mkdir -p ./docker/data/;
                              rm ./products-monitor-server/target/products-monitor-server-*-sources.jar;
                              cp ./products-monitor-server/target/products-monitor-server-*.jar ./docker/data/monitor.jar;''')
                        sh("docker build -t ${dockerUrl}:latest -t ${dockerUrl}:${dockerTagPrefix} -t ${dockerUrl}:${dockerTagPrefix}-${env.BUILD_NUMBER} --pull=true docker/")
                    }
                }

                stage('Docker Push') {
                    withDockerRegistry([url: "https://${dockerUrl}", credentialsId: dockerCredentialsId]) {
                        sh("docker push ${dockerUrl}:${dockerTagPrefix}-${env.BUILD_NUMBER}")
                        sh("docker push ${dockerUrl}:${dockerTagPrefix}")
                        sh("docker push ${dockerUrl}:latest")
                    }
                    currentBuild.description = "${dockerTagPrefix}-${env.BUILD_NUMBER} : " + currentBuild.description
                }
            }

            stage('Build With Tests') {
                // Docker build includes deployment of core artifacts.
                withMaven(maven: mavenToolName, globalMavenSettingsConfig: mavenSettings, jdk: jdkToolName, mavenOpts: "${mavenJvmOptions} -Xms128M -Xmx1024M -Xss2M") {
                    sh("mvn ${mavenOptions} clean install")
                }
            }

            stage('Maven Clean') {
                withMaven(maven: mavenToolName, globalMavenSettingsConfig: mavenSettings, jdk: jdkToolName,
                          mavenOpts: "${mavenJvmOptions} -Xmx64M") {
                    sh("mvn ${mavenOptions} clean")
                }
            }

        } catch (e) {
            bitbucketStatusNotify(buildState: 'FAILED')
            // Send failure notification.
            final duration = formatMillisecondDuration(System.currentTimeMillis() - startTime)
            final String stackTrace = getStackTrace(e);
            emailext(subject: "BUILD FAILED: ${buildName}",
                     body: """<h3>BUILD FAILED: ${buildName}</h3>
                              <p><a href="${env.BUILD_URL}">${buildName}</a> failed after ${duration} with the following error:</p>
                              <pre>${stackTrace}</pre>
                              <p>See the attached log for more information.</p>""",
                     mimeType: "text/html",
                     attachLog: true,
                     compressLog: false,
                     to: notificationRecipients.join(" "))

            // Rethrow exception to mark build as failure.
            throw e
        }


        // Send build success notification.
        final duration = formatMillisecondDuration(System.currentTimeMillis() - startTime)
        emailext(subject: "BUILD SUCCESS: ${buildName}",
                 body: """<h3>BUILD SUCCESS: ${buildName}</h3>
                          <p><a href="${env.BUILD_URL}">${buildName}</a> built successfully in ${duration}.</p>
                          <p>See the attached log for more information.</p>""",
                 mimeType: "text/html",
                 attachLog: true,
                 compressLog: true,
                 to: notificationRecipients.join(" "))
    }
    bitbucketStatusNotify(buildState: 'SUCCESSFUL')
}

static String formatMillisecondDuration(final long millis) {
    final long hours = millis.intdiv(1000).intdiv(3600)
    final long minutes = millis.intdiv(1000).intdiv(60) % 60
    final long seconds = millis.intdiv(1000) % 60
    String s = ""
    if (hours > 0) s += hours + "h "
    if (minutes > 0) s += minutes + "m "
    if (seconds > 0) s += seconds + "s "
    if (hours == 0L && minutes == 0L && seconds == 0L) s += millis + "ms"
    return s.trim()
}

@NonCPS
static String getStackTrace(final Exception e) {
    final StringWriter sw = new StringWriter();
    final PrintWriter pw = new PrintWriter(sw);
    e.printStackTrace(pw);
    return sw.toString();
}
